/**
 * Jeu de Wythoff permettant de jouer entre 2 personnes
 * @author Claire Thébault
 */
class Wythoff1{
    void principal(){
		
		//Tests méthodes
		/*testChangeJoueur();
		testCoordonnees();
		testCreerPlateau();
		testDeplacePion();
		testInitialPion();
		testJeuFini();*/
		
        jouer();
    }
    
    /**
	* Lance une partie de Wythof
	*/
    void jouer(){
		
		//Nom du premier joueur
		String joueur1 = nomJoueur("joueur 1");
		//Nom du deuxième joueur
		String joueur2 = nomJoueur("joueur 2");
		// On commence par le joueur 1 
		String joueur = joueur1;
		
        //Demande une taille de plateau à l’utilisateur
		int taille;
		do{
			taille = SimpleInput.getInt("Rentrez la taille de la longueur de la grille sur laquelle vous souhaitez jouer :");
		}while(taille > 100 || taille < 3);
		
		//Création du plateau de jeu
		char[][] grille = creerPlateau(taille);
		
		//Place le pion sur la grille 
		initialPion(grille);
		
		//Affiche le plateau initial
        affichePlateau(grille);
		
		//stocke l'emplacement du pion
        int[] coordonnees = coordonnees(grille);
        
        //Boucle de jeu : elle continue tant que la condition de fin n’est pas atteinte
        while (!jeuFini(grille)){
			
			System.out.println("Le joueur actuel est : " + joueur);
			
			//Deplace le pion comme demandé par l'utilisateur
			deplacePion(grille, coordonnees);
			
			//Stocke la nouvelle position du pion
			coordonnees = coordonnees(grille);
			
			//affiche le plateau
			affichePlateau(grille);
			
			//Si le jeu n'est pas terminé, le joueur change
			if (jeuFini(grille) == false){
				joueur = changeJoueur(joueur, joueur1, joueur2);
			}
		}
		System.out.println("Le jeu est fini ! Le gagnant est "+joueur);
    }

    /**
	* Créer un plateau de jeu carré rempli de caractere espace ’ ’
	* @param lg taille du plateau 
	* @return tableau de caractere en deux dimensions
	*/
	char[][] creerPlateau(int lg){
		char[][] plateau = new char[lg][lg];
		for (int i =0; i<plateau.length ; i++){
			for (int j =0; j <plateau[i].length; j++){
				plateau[i][j]=' ';
			}
		}
		return plateau;
	}

    /**
	* Affichage du plateau avec les indices de lignes et de colonnes
	* @param plateau le tableau a afficher
	*/
	void affichePlateau(char[][] plateau){
		for(int i = plateau.length-1; i >= 0; i--) {
			if (i < 10) {
				System.out.print(" ");
			}
			System.out.print((i) + "|");
			for(int j = 0; j < plateau.length; j++) {
				System.out.print(" " + plateau[i][j] + " |");
			}
			System.out.println();
		}
		System.out.print("  ");
		for(int k = 0; k < plateau.length; k++) {
			System.out.print("  " + k);
				if (k < 10) {
					System.out.print(" ");
				}
		}
		System.out.println();
	}
	
	/**
	* Initialise la position initiale du pion
	* @param plateau la grille de jeu où l'on place le pion
	*/
	void initialPion (char[][] plateau){
		int coordy = plateau.length-1;
		int coordx = plateau.length-1;
		
		int colOrRow = (int)(Math.random()*2);//Le pion sera placé sur la première ligne ou sur la dernière colone
		
		if (colOrRow == 0){
			coordx = (int)(Math.random()*(plateau.length-2)+1);//Le pion sera placé sur la première ligne, mais pas sur la première, ni la dernière case
			plateau[coordy][coordx]='o';
		}
		else {
			coordy = (int)(Math.random()*(plateau.length-2)+1);//Le pion sera placé sur la dernière colone, mais pas sur la première, ni la dernière case
			plateau[coordy][coordx]='o';
		}
	}
	 
	/**
	* Permet d'obtenir la position du pion
	* @param plateau plateau où est le pion 
	* @return coordonnees position du pion sur le plateau
	*/
	int[] coordonnees (char[][] plateau){
		int coordi = 0;
		int coordj = 0;
		int[] coord = new int[2];
		  
		for (int i = 0 ; i< plateau.length; i++){
			int j = 0;
			while(j < plateau[i].length){
				if (plateau[i][j] =='o'){
					coordi = i;
					coordj = j;
				}
				j++;
			}
		}
		coord[0] = coordi;
		coord[1] = coordj;
		  
		  return coord;
	  }

	/**
	 * Permet de définir le nom d'un joueur
	 * @param joueur joueur dont on veut rentrer le nom
	 * @return nomJoueur renvoie le nom choisi par l'utilisateur
	 */
	String nomJoueur(String joueur) {
		String nom;
		nom = SimpleInput.getString("Entrez le nom du " + joueur + " : ");

		return nom;
	}
	
	/**
	* Change le joueur actuel
	* @param joueurInitial est le joueur qui vient de joeur au dernier tour
	* @param joueur1 est le premier joueur
	* @param joueur2 est le deuxième joueur
	* @return si le joueur1 est en parametre alors renvoie joueur2 sinon renvoie joueur1
	*/
	String changeJoueur(String joueurInitial, String joueur1, String joueur2){
		String joueur = joueurInitial;
		if (joueur == joueur1){
			joueur = joueur2;
		}
		else {
			joueur = joueur1;
		}
		return joueur;
	}

    /**
     * Déplace le pion dans la direction et du nombres de cases demandées par le joueur 
     * @param plateau est la grille sur laquelle on va afficher le pion
     * @param coordonnees contient la position actuelle du pion
     * @return coordonnees on stock la nouvelle position du pion
     */
    void deplacePion(char[][] plateau, int[]coordonnees){
		int y = coordonnees[0];
		int x = coordonnees[1];
		char direction;
		int nbCases;
		 
		plateau[y][x]=' ';//Efface le pion à son ancien emplacement
		
		// verifier coordonnees si le pion est coller au bord gauche  alors les déplacements à gauche et en diagonale ne sont pas possibles 
		if (x == 0){
			direction = 'b';
			System.out.println("Seul un deplacement vers le bas est possible");
			
		// verifier coordonnees si le pion est coller au bord du bas alors les déplacements en bas et en diagonale ne sont pas possibles  
		} else if (y == 0){
			direction= 'g';
			System.out.println("Seul un deplacement vers la gauche est possible");
			
		}else {
			do {
				direction = SimpleInput.getChar("Dans quelle direction souhaitez vous vous deplacer ? g=gauche, b=bas, d=diagonal : ");
			} while (direction !='g' && direction!='b' && direction!='d');
		}
		if(direction =='g'){
			do {
				nbCases = SimpleInput.getInt("De combien de cases souhaitez vous vous deplacer ? : ");
			} while(x-nbCases<0);
			plateau[y][x-nbCases]='o';
		} else if (direction =='b'){
			do {
				nbCases = SimpleInput.getInt("De combien de cases souhaitez vous vous deplacer ? : ");
			} while(y-nbCases<0);
			plateau[y-nbCases][x]='o';
		} else {
			do {
				nbCases = SimpleInput.getInt("De combien de cases souhaitez vous vous deplacer ? : ");
			} while(y-nbCases<0 || x-nbCases<0);
			plateau[y-nbCases][x-nbCases]='o';
		}		
	}
	 
	/**
	 * On vérifie si la partie est finie
	 * @param plateau on vérifie si la case (0,0) est prise
	 * @return fini s'il y a un gagnant on renvoie que la partie est finie, sinon qu'elle ne l'est pas
	 */
	boolean jeuFini(char[][] plateau){
		boolean fini = false;
		if (plateau[0][0]== 'o'){
			fini = true;
		}
		return fini;
	}
	
	
	//***Méthodes de test***
	
	/**
	 * Teste la méthode changeJoueur()
	 */ 
	 void testChangeJoueur () {
		System.out.println ();
		System.out.println ("*** testChangeJoueur()");
		testCasChangeJoueur ("joueur1", "joueur1", "joueur2", "joueur2" );
		testCasChangeJoueur ("Claire", "Ewen", "Claire", "Ewen" );
		}
	/**
	* teste un appel de changeJoueur
	* @param joueurInitial joueur actuel que l'on souhaite changer
	* @param nom du premier joueur
	* @param nom du deuxième joueur 
	* @param result resultat attendu
	*/
	
	void testCasChangeJoueur (String joueurInitial, String joueur1, String joueur2, String result) {
		// Arrange
		System.out.print ("joueur initial (" + joueurInitial + ") apres methode \t: " + result + "\t : ");
		// Act
		String resExec = changeJoueur(joueurInitial, joueur1, joueur2);
		// Assert
		if (resExec == result){
			System.out.println ("OK");
		} else {
			System.err.println ("ERREUR");
		}
	}

	 /**
	 * Teste la méthode creerPlateau()
	 */
	 
	 void testCreerPlateau() {
		System.out.println ();
		System.out.println ("*** testCreerPlateau()");
        char [][] result1 = {{' ',' ',' '},{' ',' ',' '},{' ',' ',' '},{' ',' ',' '},{' ',' ',' '},{' ',' ',' '}};
        testCasCreerPlateau(6, result1);
        
        char [][] result2 = {{' ',' '},{' ',' '}};
        testCasCreerPlateau(2, result2);
        
        char [][] result3 = {{' '}};
        testCasCreerPlateau(1, result3);
    }
	
	/**
	* teste un appel de creerPlateau
	* @param lg longueur et largeur du plateau voulu
	* @param result resultat attendu
	*/
    void testCasCreerPlateau(int lg, char[][] result){
        //Arrange
        System.out.print("La fonction renvoie bien un tableau de taille "+lg+" : ");

        //Act
        char [][] resExec=creerPlateau(lg);
        System.out.println();
        affichePlateau(resExec);

        //Assert
        if (resExec.length == result.length){
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }
	 
	/**
	 * Teste la méthode deplacePion()
	 */
    void testDeplacePion (){
        System.out.println();
        System.out.println("*** testDeplacePion()");

        char [][] plateau = {{' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ',' ',' ','o',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '}};
        int[] coordonnees = {3,3};
        int[] result = {0,0};
        testCasDeplacePion(plateau, coordonnees, result);
    }

	/**
	* teste un appel de deplacePion()
	* @param plateau plateau ou l'on deplace le pion 
	* @param coordonnees position du pion
	* @param result resultat attendu : nouvelle position du pion
	*/
    void testCasDeplacePion(char[][] plateau, int[] coordonnees, int[] result){
    //Arrange
        System.out.println("La fonction deplacePion change la position du pion de (3,3) a (0,0) avec un input (d,3): ");
		System.out.println("On peut voir le plateau avant deplacePion avec la fonction affichePlateau : ");
		affichePlateau(plateau);
        //Act
        deplacePion(plateau, coordonnees);
        coordonnees = coordonnees(plateau);// On actualise les coordonnees du pion après éxécution de deplacePion
        System.out.println("Voici le plateau après deplacePion  : ");
        affichePlateau(plateau);
        int [] resExec = coordonnees;

        //Assert
        if (resExec[0] == result[0] && resExec[1] == result[1]){
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }
    
	 
	/**
	 * Teste la méthode initialPion()
	 */
	 void testInitialPion () {
		System.out.println ();
		System.out.println ("*** testInitialPion()");
		System.out.println ("Cette methode fait appel a Math.random(), il est donc complique de la tester, si lorsque on l appelle plusieurs fois elle nous donne des resultats differents on considerera que elle est correcte");
		
		testCasInitialPion (creerPlateau(6));
		testCasInitialPion (creerPlateau(6));
		}
		
	/**
	* teste un appel de initialPion
	* @param plateau plateau ou l'on place un pion de manière aléatoire sur la première ligne ou sur la dernière colone
	* @param result resultat attendu
	*/
	void testCasInitialPion (char[][] plateau) {
		initialPion(plateau);
		affichePlateau(plateau);
		System.out.println();
		
	}
	 
	/**
	 * Teste la méthode coordonnees()
	 */
	void testCoordonnees() {
        System.out.println();
        System.out.println("*** testCoordonnees()");

        // Création des plateaux de test.
        char[][] plateau1 = {{' ',' ',' ',' ',' '},
                        {' ',' ',' ',' ',' '},
                        {' ',' ',' ',' ',' '},
                        {'o',' ',' ',' ',' '},
                        {' ',' ',' ',' ',' '}};

        int[] result1 = {3,0};

        char[][] plateau2 = {{' ',' ',' ',' ',' '},
                        {' ',' ',' ',' ',' '},
                        {' ',' ',' ',' ',' '},
                        {' ',' ',' ',' ',' '},
                        {' ',' ',' ','o',' '}};
        int[] result2 = {4,3};

        testCasCoordonnees(plateau1,result1);
        testCasCoordonnees(plateau2,result2);

    }
    
    /**
	* teste un appel de coordonnees()
	* @param plateau tableau ou l'on cherche la position d'un pion
	* @param result resultat attendu
	*/
	void testCasCoordonnees(char[][] plateau, int[] resultat) {
        //Arrange
        System.out.println();
        affichePlateau(plateau);
        System.out.print("La methode determine les coordonnees du pion (" + resultat[0] + "," + resultat[1] + ") : " );
		
        //Act
        int[] resExec = coordonnees(plateau);

        //Assert
        if (resExec[0] == resultat[0] && resExec[1] == resultat[1]) {
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }
	 
	 
	/**
	 * Teste la méthode jeuFini()
	 */
	void testJeuFini (){
        System.out.println();
        System.out.println("*** testJeuFini");

        char [][] plateau1 = {{' ',' ',' ',' ',' ',' '},
            {' ',' ','o',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '}};
	   char [][] plateau2 = {{'o',' ',' ',' ',' ',' '},
			{' ',' ',' ',' ',' ',' '},
			{' ',' ',' ',' ',' ',' '},
			{' ',' ',' ',' ',' ',' '},
			{' ',' ',' ',' ',' ',' '},
			{' ',' ',' ',' ',' ',' '}};
			
        testCasJeuFini(plateau1, false);
        testCasJeuFini(plateau2, true);
    }

	/**
	* teste un appel de jeuFini
	* @param plateau tableau ou l'on vérifie si la partie est terminée
	* @param result resultat attendu
	*/
    void testCasJeuFini(char[][] plateau, boolean result){
		//Arrange
		System.out.println("Avec la fonction affichePlateau on peut voir le tableau ou la fonction jeuFini doit retourner \t: " + result + "\t : ");
		affichePlateau(plateau);
		
		//Act
		boolean resExec = jeuFini(plateau);

		//Assert
		if (resExec == result){
			System.out.println("OK");
		} else {
			System.err.println("ERREUR");
		}
    }	 
}
