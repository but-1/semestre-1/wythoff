/**
 * Programme pour jouer à Wythoff contre un ordinateur
 * @author Claire Thébault
 */
class Wythoff2{
    void principal(){
		
		/*
		//Méthodes de test
		testPositionGagnantes();
		testEstGagnante();
		testNbPasUtilise();
		testDeplacePionOrdinateur();
		
		//Méthodes de test de Wythoff1
		testChangeJoueur();
		testCoordonnees();
		testCreerPlateau();
		testDeplacePionJoueur();
		testInitialPion();
		testJeuFini();
		*/
		
        jouer();
    }
    
    /**
	* Lance une partie de Wythof
	*/
    void jouer(){
		//Nom du premier joueur
		String joueur1 = nomJoueur("joueur1");
		//Nom du deuxième joueur
		String joueur2 = "Ordinateur";
		// On choisi le 1er joueur
		String[] nomsJoueurs = {joueur1, joueur2};
		int nom;
		
		do {
			nom = SimpleInput.getInt("Choisissez le premier a jouer entre l'ordinateur et vous (0 = Vous et 1 = Ordinateur) : ");
		} while (nom < 0 || nom>1);
		
		//Défini le joueur actuel
		String joueur = nomsJoueurs[nom];
		
        //Demande une taille de plateau à l’utilisateur
		int taille;
		do{
			taille = SimpleInput.getInt("Rentrez la taille de la longueur de la grille sur laquelle vous souhaitez jouer :");
		}while(taille > 100 || taille < 3);
		
		//Création du plateau de jeu
		char[][] grille = creerPlateau(taille);
		
		//Initialise les positions gagnantes
        int[][] posGagnantes = positionGagnantes(grille);
		
		//Affiche les positions gagnantes sur le plateau
        affichagePosGagn(posGagnantes, grille);
		
		//Place le pion sur la grille 
		initialPion(grille);
        
		//Affiche le plateau initial
        affichePlateau(grille);
		
		//stocke l'emplacement du pion
        int[] coordonnees = coordonnees(grille);
        
        //Boucle de jeu : elle continue tant que la condition de fin n’est pas atteinte
        while (!jeuFini(grille)){
			
			System.out.println("C'est au tour de : " + joueur);
			
			//Deplace le pion 
			if (joueur == "Ordinateur"){
				deplacePionOrdinateur(grille, coordonnees, posGagnantes);
			} else { 
				deplacePionJoueur(grille, coordonnees, posGagnantes);
			}
			
			//Stocke la nouvelle position du pion
			coordonnees = coordonnees(grille);
			
			//affiche le plateau
			affichePlateau(grille);
			
			//Si le jeu n'est pas terminé, le joueur change
			if (jeuFini(grille) == false){
				joueur = changeJoueur(joueur, joueur1, joueur2);
			}
		}
		System.out.println("Le jeu est fini ! Le gagnant est "+joueur);
    }

    /**
	* Créer un plateau de jeu carré rempli de caractere espace ’ ’
	* @param lg taille du plateau 
	* @return tableau de caractere en deux dimensions
	*/
	char[][] creerPlateau(int lg){
		char[][] plateau = new char[lg][lg];
		for (int i =0; i<plateau.length ; i++){
			for (int j =0; j <plateau[i].length; j++){
				plateau[i][j]=' ';
			}
		}
		return plateau;
	}

    /**
	* Affichage du plateau avec les indices de lignes et de colonnes
	* @param plateau le tableau a afficher
	*/
	void affichePlateau(char[][] plateau){
		for(int i = plateau.length-1; i >= 0; i--) {
			if (i < 10) {
				System.out.print(" ");
			}
			System.out.print((i) + "|");
			for(int j = 0; j < plateau.length; j++) {
				if (plateau[i][j] == 'o') {
					System.out.print(" " + plateau[i][j] + " |");
				} else {
					System.out.print(" " + plateau[i][j] + " |");
				}
			}
			System.out.println();
		}
		System.out.print("  ");
		for(int k = 0; k < plateau.length; k++) {
			System.out.print("  " + k);
				if (k < 10) {
					System.out.print(" ");
				}
		}
		System.out.println();
	}
	
	/**
	* Initialise la position initiale du pion
	* @param plateau la grille de jeu où l'on place le pion
	* @return coord renvoie la position du pion
	*/
	void initialPion (char[][] plateau){
		int coordy = plateau.length-1;
		int coordx = plateau.length-1;
		
		int colOrRow = (int)(Math.random()*2);//Le pion sera placé sur la première ligne ou sur la dernière colone
		
		if (colOrRow == 0){
			coordx = (int)(Math.random()*(plateau.length-2)+1);
			plateau[coordy][coordx]='o';
		}
		else {
			coordy = (int)(Math.random()*(plateau.length-2)+1);
			plateau[coordy][coordx]='o';
		}
	}
	 
	/**
	* Permet d'obtenir la position du pion
	* @param plateau plateau où est le pion 
	* @return coordonnees position du pion sur le plateau
	*/
	int[] coordonnees (char[][] plateau){
		int coordi = 0;
		int coordj = 0;
		int[] coord = new int[2];
		  
		for (int i = 0 ; i< plateau.length; i++){
			int j = 0;
			while(j < plateau[i].length){
				if (plateau[i][j] =='o'){
					coordi = i;
					coordj = j;
				}
				j++;
			}
		}
		coord[0] = coordi;
		coord[1] = coordj;
		  
		  return coord;
	  }

	/**
	* Permet de définir le nom d'un joueur
	* @param joueur joueur dont on veut rentrer le nom
	*/
	String nomJoueur(String joueur) {
		  String nom;
		  nom = SimpleInput.getString("Entrez le nom du " + joueur + " : ");

		  return nom;
	}
	
	/**
	* Change le joueur actuel
	* @param joueurInitial est le joueur qui vient de joeur au dernier tour
	* @param joueur1 est le premier joueur
	* @param joueur2 est le deuxième joueur
	* @return si le joueur1 est en parametre alors renvoie joueur2 sinon renvoie joueur1
	*/
	String changeJoueur(String joueurInitial, String joueur1, String joueur2){
		String joueur = joueurInitial;
		if (joueur == joueur1){
			joueur = joueur2;
		}
		else {
			joueur = joueur1;
		}
		return joueur;
	}
	
	/**
     * Déplace le pion dans la direction et du nombres de cases demandées par le joueur 
     * @param plateau est la grille sur laquelle on va afficher le pion
     * @param coordonnees contient la position actuelle du pion
     * @param positions contient les positions gagnantes (permet de les réafficher sur le tableau si besoin)
     */
    void deplacePionJoueur(char[][] plateau, int[]coordonnees, int[][] positions){
		int y = coordonnees[0];
		int x = coordonnees[1];
		char direction;
		int nbCases;
		 
		if (estGagnante(positions, coordonnees)){
			plateau[y][x]='x';//Si le pion se trouvait sur une position gagnante alors on remplace le pion par 'x'
		} else {
			plateau[y][x]=' ';//Efface le pion à son ancien emplacement
		}
		
		// verifier coordonnees si x==0 => g et d pas possible 
		if (x == 0){
			direction = 'b';
			System.out.println("Seul un deplacement vers le bas est possible");
			
		// verifier coordonnees si y== 0 => b et d pas possible 
		} else if (y == 0){
			direction= 'g';
			System.out.println("Seul un deplacement vers la gauche est possible");
			
		}else {
			do {
				direction = SimpleInput.getChar("Dans quelle direction souhaitez vous vous deplacer ? g=gauche, b=bas, d=diagonal : ");
			} while (direction !='g' && direction!='b' && direction!='d');
		}
		if(direction =='g'){
			do {
				nbCases = SimpleInput.getInt("De combien de cases souhaitez vous vous deplacer ? : ");
			} while(x-nbCases<0);
			plateau[y][x-nbCases]='o';
		} else if (direction =='b'){
			do {
				nbCases = SimpleInput.getInt("De combien de cases souhaitez vous vous deplacer ? : ");
			} while(y-nbCases>plateau.length-1);
			plateau[y-nbCases][x]='o';
		} else {
			do {
				nbCases = SimpleInput.getInt("De combien de cases souhaitez vous vous deplacer ? : ");
			} while(y-nbCases>plateau.length-1 || x-nbCases<0);
			plateau[y-nbCases][x-nbCases]='o';
		}		
	}
	
	/**
	 * Permet de savoir si une position est gagnante ou non
	 * @param positions tableau contenant toutes les positions gagnantes
	 * @param coordonnees contient la position du pion, si la position est présente dans le tableau position, alors le pion est sur une position gagnante
	 * @return estGagnante Renvoie vraie si le pion se situe sur une position gagnante, faux si ça n'est pas le cas
	 */
	boolean estGagnante(int[][]positions, int[]coordonnees){ 
		boolean estGagnante = false;
		for(int i=0; i<positions.length; i++){
			if(positions[i][0] == coordonnees[0]){
				if(positions[i][1] == coordonnees[1]){
					estGagnante = true;
				}
			}
		}
		return estGagnante;
	}
	
    /**
     * Déplace le pion dans la direction et du nombres de cases demandées par le joueur 
     * @param plateau est la grille sur laquelle on va afficher le pion
     * @param coordonnees contient la position actuelle du pion
     * @return coordonnees on stock la nouvelle position du pion
     */
    void deplacePionOrdinateur(char[][] plateau, int[]coordonnees, int[][]positions){
		int y = coordonnees[0];
		int x = coordonnees[1];
		char direction;
		char[] directionPossibles = {'g', 'b', 'd'};
		int directionJoueur;
		
		boolean positionAbscisse =false;
		boolean positionOrdonnee =false;
		boolean positionDiagonale =false;
		
		int nbCases = 1;
		int tmpI = 1;
		int tmpJ = 1;				

			//on vérifie si il y a une position gagnante sur la meme ligne et à gauvche du pion
			for(int i=0; i<positions.length; i++){				
				if (positions[i][0]== y && positions[i][1] < x){
					positionAbscisse = true;
					tmpI =i;
				}
			}
			
			//on vérifie si il y a une position gagnante sur la meme colone et en dessous du pion
			for(int i=0; i<positions.length; i++){
				if (positions[i][1]== x && positions[i][0] < y){
					positionOrdonnee = true;
					tmpI =i;
				}
			}
			
			//on vérifie si il y a une position gagnante dans la diagonale du pion et en dessous
			for(int i=0; i<positions.length; i++){
				for(int j=1; j<plateau.length; j++){
					if (positions[i][0]== y-j && positions[i][1] == x-j){
						positionDiagonale = true;
						tmpI =i;
						tmpJ = j;
					}
				}
			}
			
			if(positionAbscisse){//S'il existe une position gagnante sur la meme abscisse et abscisse inférieure à la sienne choisir g
				direction = directionPossibles[0];
				nbCases = x-positions[tmpI][1];
				
			}else if(positionOrdonnee){//S'il existe une position gagnante sur la meme ordonnée et ordonnée inférieure à la sienne choisir b
				direction = directionPossibles[1];
				nbCases = y-positions[tmpI][0];
				
			}else if(positionDiagonale){//S'il existe une position gagnante sur son ordonné - n et son abcisse - n
				direction = directionPossibles[2];
				nbCases = tmpJ;
				
			} else{
				//S'il n'y a pas de stratégie gagnantes accessible ni de déplacement contraints  l'ordinateur joue au hasard
				int randomDirection = (int)(Math.random()*3);
				direction = directionPossibles[randomDirection];
				if(direction =='g'){
					do {
						nbCases = (int)(Math.random()*(plateau.length-1)+1);
					} while(x-nbCases<0);
				}
				else if (direction =='b'){
					do {
						nbCases = (int)(Math.random()*(plateau.length-1)+1);
					} while(y-nbCases<0);
				}
				else {
					
					do {
						nbCases = (int)(Math.random()*(plateau.length-1)+1);
					} while(y-nbCases<0 || x-nbCases<0);
				
				}
			}
		
		//Efface le pion à son ancien emplacement et le remplace par 'x' si c'est une positions gagnante ou par un espace vide
		if (estGagnante(positions, coordonnees)){
			plateau[y][x]='x';
		} else {
			plateau[y][x]=' '; 
		}
		
		//Met le pion à la position voulue
		if(direction =='g'){
			plateau[y][x-nbCases]='o';
		}
		 else if (direction =='b'){
			plateau[y-nbCases][x]='o';
		}
		else {
			plateau[y-nbCases][x-nbCases]='o';
		}
	}
	 
	/**
	 * On vérifie si la partie est finie
	 * @param plateau on vérifie si la case (0,0) est prise
	 * @return fini s'il y a un gagnant on renvoie que la partie est finie, sinon qu'elle ne l'est pas
	 */
	boolean jeuFini(char[][] plateau){
		boolean fini = false;
		if (plateau[0][0]== 'o'){
			fini = true;
		}
		return fini;
	}
	
	/**
	 * Permet de connaitre le nombre le plus petit qu n'est pas encore présent dans un tableau à double dimension
	 * @param positions tableau dont on veut savoir quel est le plus petit nombre qui n'y est pas présent
	 * @return nbPasUtilise retourne le plus petit nombre au dessus de 0 qui n'est pas présent dans le tableau
	 */
	//Tant que le nombre nbPas Utilisé apparait dans notre tableau position, on veut augmenter nbPas utilisé
	int nbPasUtilise(int[][] positions){
		int nbPasUtilise = 0;
		for(int i=0; i< positions.length; i++){
			for(int j=0; j<positions[i].length; j++){
				while(nbPasUtilise == positions[i][j]){
					nbPasUtilise ++;
				}
				for(int k=0; k< positions.length; k++){
					for(int l=0; l<positions[k].length; l++){
						while(nbPasUtilise == positions[k][l]){
							nbPasUtilise ++;
						}
					}
				}
			}
		}
		return nbPasUtilise;
	}
	
	/**
	 * Détermine les positions gagnantes d'un plateau donné
	 * @param plateau plateau dont on veut connaitre les positions gagnantes
	 * @return positions renvoie les positions gagnantes
	 */
	int[][] positionGagnantes(char[][]plateau){
		int max = plateau.length*2;// A revoir
		int[][] positions = new int [max][2];
		int x = 0;
		int y = 0;
		int rang = 0;
		int nbPasUtilise = 0;
		
		for (int i = 0; i< plateau.length/2; i++){
			while(x<plateau.length){
				if(x == nbPasUtilise){
					y = rang + x;
					positions[i][0]= x;
					positions[i][1]= y;
					rang ++;
					i++;
				}
				nbPasUtilise = nbPasUtilise(positions);
				x++;
			}
		}
		/*Symétrie : la première partie de la fonction permet de connaitre 
		 * les positions gagnantes de la partie supérieure du plateau, par 
		 * symétrie on peut connaitre les positions gagnantes de la partie inférieure.*/
		int j = 0;
		for(int i = positions.length/2; i<positions.length; i++){
			positions[i][0]= positions[j][1];
			positions[i][1]= positions[j][0];
			j++;
		}

		return positions;
	}
	 
	/**
	 * Affiche les positions gagnantes avec un 'x' sur le plateau
	 * @param positionGagnantes donne les positions gagnantes de plateau
	 * @param plateau tableau qui sert à afficher le plateau, on remplace les caractères ' ' par 'x'
	 */
	void affichagePosGagn(int[][]positionGagnantes, char[][] plateau){
		
		for(int i = 0; i<positionGagnantes.length ; i++){
			int x = positionGagnantes[i][0];
			int y = positionGagnantes[i][1];
			if(y<plateau.length && x<plateau.length){
				plateau[y][x]='x';			
			}
		}
	}
	 
	//Méthodes de test
	/**
	 * Teste la méthode estGagnante.
	 */
	void testEstGagnante() {
		System.out.println();
        System.out.println("*** testEstGagnante");
         
        int[][]positions = {{0,0},{1,2},{3,5},{4,7},{6,10},{0,0},{2,1},{5,3},{7,4},{10,6}};
        int[] coordonnees1 = {1, 2};
        int[] coordonnees2 = {2, 3};

        testCasEstGagnante(positions, coordonnees1, true);
        testCasEstGagnante(positions, coordonnees2, false);

	}
	 
	/**
	 * Teste un appel de la méthode estGagnante()
	 */
	void testCasEstGagnante(int[][] positions, int[] coordonnees, boolean result) {
		// Arrange
		System.out.print("Le pion avec comme coordonnees ");
		displayTab1(coordonnees);
		System.out.println(" est sur une position gagnante ? \t : "+result);
		 
		//Act
        boolean resExec = estGagnante(positions, coordonnees);

        //Assert
        if (resExec == result) {
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
        System.out.println();
	}
	
	/**
	 * Teste la méthode positionGagnantes()
	 */
	void testPositionGagnantes() {
        System.out.println();
        System.out.println("*** testPositionGagnantes()");
		
		int[][] positions6 = {{0,0},{1,2},{3,5},{4,7},{0,0},{0,0},{0,0},{2,1},{5,3},{7,4},{0,0},{0,0}};
		int[][] positions10 = {{0,0},{1,2},{3,5},{4,7},{6,10},{8,13},{9,15},{0,0},{0,0},{0,0},{0,0},{2,1},{5,3},{7,4},{10,6},{13,8},{15,9},{0,0},{0,0},{0,0}};
		
		int lg1 = 6;
		char[][] plateau1 = creerPlateau(lg1);
		int lg2 = 10;
		char[][] plateau2 = creerPlateau(lg2);
        
        testCasPositionGagnantes(plateau1,positions6);
        testCasPositionGagnantes(plateau2,positions10);
    }
    
    /**
	* teste un appel de positionGagnantes()
	* @param plateau tableau ou l'on cherche la position d'un pion
	* @param result resultat attendu
	*/
	void testCasPositionGagnantes(char[][] plateau, int[][] resultat) {
        //Arrange
        System.out.println();
        System.out.println("Etant donne que la methode positionGagnante cree un tableau plus grand que necessaire le resultat attendu est : " );
		displayTab(resultat);
		System.out.println();
		System.out.println();
		
        //Act
        int[][] resExec = positionGagnantes(plateau);
        System.out.println("La fonction positionGagnantes renvoie : ");
        displayTab(resExec);
        System.out.println();

        //Assert
        boolean erreur = false;
        for(int i =0; i<resultat.length; i++){
			if (resExec[i][0] != resultat[i][0] || resExec[i][1] != resultat[i][1]){
				erreur = true;
			}
		}
        if (!erreur) {
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }
		
	
	/**
	 * Teste la méthode nbPasUtilise()
	 */
	void testNbPasUtilise(){
		System.out.println();
        System.out.println("*** testNbPasUtilise()");
        
        int [][]positions1 = {{0,0},{1,2},{3,5},{4,7}};
        int [][]positions2 = {{0,1},{4,7}};
        
        testCasNbPasUtilise(positions1, 6);
        testCasNbPasUtilise(positions2, 2);
        
		
	}
	
	/**
	* teste un appel de nbPasutilise()
	* @param positions
	* @param result resultat attendu
	*/
	void testCasNbPasUtilise(int[][] positions, int result){
		// Arrange
		System.out.print("Dans le tableau suivant : ");
		displayTab(positions);
		System.out.print(", le plus petit nombre pas utilise est "+result);
		
		 
		//Act
        int resExec = nbPasUtilise(positions);
		System.out.println(" la methode nbPasUtilise renvoie : "+ resExec );
        //Assert
        if (resExec == result) {
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
        System.out.println();
	}
	
	
	/**
	 * Teste la méthode deplacePionOrdinateur()
	 */
    void testDeplacePionOrdinateur (){
        System.out.println();
        System.out.println("*** testDeplacePionOrdinateur");

		int[][] positions = {{0,0},{1,2},{3,5},{4,7},{6,10},{0,0},{2,1},{5,3},{7,4},{10,6}};
        char [][] plateau1 = {{' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ',' ',' ','o',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '}};
        int[] coordonnees1 = {3,3};
        int[] result1 = {0,0};
        
        char [][] plateau2 = {{' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ','o',' ',' ',' ',' '}};
        int[] coordonnees2 = {5,1};
        int[] result2 = {2,1};
        
        testCasDeplacePionOrdinateur(plateau1, coordonnees1, positions, result1);
        testCasDeplacePionOrdinateur(plateau2, coordonnees2, positions, result2);
    }

	/**
	* teste un appel de deplacePionOrdinateur()
	* @param plateau plateau ou l'on deplace le pion 
	* @param coordonnees position du pion
	* @param positions positions gagnantes
	* @param result resultat attendu : nouvelle position du pion
	*/
    void testCasDeplacePionOrdinateur(char[][] plateau, int[] coordonnees, int[][] positions, int[] result){
    //Arrange
        System.out.print("La fonction deplacePion change la position du pion de : " );
        displayTab1(coordonnees);
        System.out.print(" a ");
        displayTab1(result);
        System.out.println(" en suivant la strategie gagnante : ");
		System.out.println("On peut voir le plateau avant deplacePionOrdinateur avec la fonction affichePlateau : ");
		affichagePosGagn(positions, plateau);
		affichePlateau(plateau);
        //Act
        deplacePionOrdinateur(plateau, coordonnees, positions);
        coordonnees = coordonnees(plateau);// On actualise les coordonnees du pion après éxécution de deplacePion
        System.out.println("Voici le plateau apres deplacePionOrdinateur  : ");
        affichePlateau(plateau);
        int [] resExec = coordonnees;

        //Assert
        if (resExec[0] == result[0] && resExec[1] == result[1]){
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
        System.out.println();
    }
	
	
	
	//***Méthodes de test déjà présente dans Wythoff1***
	/**
	 * Teste la méthode changeJoueur()
	 */ 
	 void testChangeJoueur () {
		System.out.println ();
		System.out.println ("*** testChangeJoueur()");
		testCasChangeJoueur ("joueur1", "joueur1", "joueur2", "joueur2" );
		testCasChangeJoueur ("Claire", "Ewen", "Claire", "Ewen" );
		}
	/**
	* teste un appel de changeJoueur
	* @param joueurInitial joueur actuel que l'on souhaite changer
	* @param nom du premier joueur
	* @param nom du deuxième joueur 
	* @param result resultat attendu
	*/
	
	void testCasChangeJoueur (String joueurInitial, String joueur1, String joueur2, String result) {
		// Arrange
		System.out.print ("joueur initial (" + joueurInitial + ") apres methode \t: " + result + "\t : ");
		// Act
		String resExec = changeJoueur(joueurInitial, joueur1, joueur2);
		// Assert
		if (resExec == result){
			System.out.println ("OK");
		} else {
			System.err.println ("ERREUR");
		}
	}

	 /**
	 * Teste la méthode creerPlateau()
	 */
	 
	 void testCreerPlateau() {
		System.out.println ();
		System.out.println ("*** testCreerPlateau()");
        char [][] result1 = {{' ',' ',' '},{' ',' ',' '},{' ',' ',' '},{' ',' ',' '},{' ',' ',' '},{' ',' ',' '}};
        testCasCreerPlateau(6, result1);
        
        char [][] result2 = {{' ',' '},{' ',' '}};
        testCasCreerPlateau(2, result2);
        
        char [][] result3 = {{' '}};
        testCasCreerPlateau(1, result3);
    }
	
	/**
	* teste un appel de creerPlateau
	* @param lg longueur et largeur du plateau voulu
	* @param result resultat attendu
	*/
    void testCasCreerPlateau(int lg, char[][] result){
        //Arrange
        System.out.print("La fonction renvoie bien un tableau de taille "+lg+" : ");

        //Act
        char [][] resExec=creerPlateau(lg);
        System.out.println();
        affichePlateau(resExec);

        //Assert
        if (resExec.length == result.length){
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }
	 
	/**
	 * Teste la méthode deplacePionJoueur()
	 */
    void testDeplacePionJoueur (){
        System.out.println();
        System.out.println("*** testDeplacePionJoueur");

		int[][] positions = {{0,0},{1,2},{3,5},{4,7},{6,10},{0,0},{2,1},{5,3},{7,4},{10,6}};
        char [][] plateau = {{' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ',' ',' ','o',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '}};
        int[] coordonnees = {3,3};
        int[] result = {0,0};
        testCasDeplacePionJoueur(plateau, coordonnees, positions, result);
    }

	/**
	* teste un appel de deplacePionJoueur()
	* @param plateau plateau ou l'on deplace le pion 
	* @param coordonnees position du pion
	* @param positions positions gagnantes
	* @param result resultat attendu : nouvelle position du pion
	*/
    void testCasDeplacePionJoueur(char[][] plateau, int[] coordonnees, int[][] positions, int[] result){
    //Arrange
        System.out.println("La fonction deplacePion change la position du pion de (3,3) a (0,0) avec un input (d,3): ");
		System.out.println("On peut voir le plateau avant deplacePion avec la fonction affichePlateau : ");
		affichePlateau(plateau);
        //Act
        deplacePionJoueur(plateau, coordonnees, positions);
        coordonnees = coordonnees(plateau);// On actualise les coordonnees du pion après éxécution de deplacePion
        System.out.println("Voici le plateau après deplacePion  : ");
        affichePlateau(plateau);
        int [] resExec = coordonnees;

        //Assert
        if (resExec[0] == result[0] && resExec[1] == result[1]){
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }
    
	 
	/**
	 * Teste la méthode initialPion()
	 */
	 void testInitialPion () {
		System.out.println ();
		System.out.println ("*** testInitialPion()");
		System.out.println ("Cette methode fait appel a Math.random(), il est donc complique de la tester, si lorsque on l appelle plusieurs fois elle nous donne des resultats differents on considerera que elle est correcte");
		
		testCasInitialPion (creerPlateau(6));
		testCasInitialPion (creerPlateau(6));
		}
		
	/**
	* teste un appel de changeJoueur
	* @param plateau plateau ou l'on place un pion de manière aléatoire sur la première ligne ou sur la dernière colone
	*/
	void testCasInitialPion (char[][] plateau) {
		initialPion(plateau);
		affichePlateau(plateau);
		System.out.println();
		
	}
	 
	/**
	 * Teste la méthode coordonnees()
	 */
	void testCoordonnees() {
        System.out.println();
        System.out.println("*** testCoordonnees()");

        // Création des plateaux de test.
        char[][] plateau1 = {{' ',' ',' ',' ',' '},
                        {' ',' ',' ',' ',' '},
                        {' ',' ',' ',' ',' '},
                        {'o',' ',' ',' ',' '},
                        {' ',' ',' ',' ',' '}};

        int[] result1 = {3,0};

        char[][] plateau2 = {{' ',' ',' ',' ',' '},
                        {' ',' ',' ',' ',' '},
                        {' ',' ',' ',' ',' '},
                        {' ',' ',' ',' ',' '},
                        {' ',' ',' ','o',' '}};
        int[] result2 = {4,3};

        testCasCoordonnees(plateau1,result1);
        testCasCoordonnees(plateau2,result2);

    }
    
    /**
	* teste un appel de coordonnees()
	* @param plateau tableau ou l'on cherche la position d'un pion
	* @param result resultat attendu
	*/
	void testCasCoordonnees(char[][] plateau, int[] resultat) {
        //Arrange
        System.out.println();
        affichePlateau(plateau);
        System.out.print("La methode determine les coordonnees du pion (" + resultat[0] + "," + resultat[1] + ") : " );
		
        //Act
        int[] resExec = coordonnees(plateau);

        //Assert
        if (resExec[0] == resultat[0] && resExec[1] == resultat[1]) {
            System.out.println("OK");
        } else {
            System.err.println("ERREUR");
        }
    }
	 
	 
	/**
	 * Teste la méthode jeuFini()
	 */
	void testJeuFini (){
        System.out.println();
        System.out.println("*** testJeuFini");

        char [][] plateau1 = {{' ',' ',' ',' ',' ',' '},
            {' ',' ','o',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '},
            {' ',' ',' ',' ',' ',' '}};
	   char [][] plateau2 = {{'o',' ',' ',' ',' ',' '},
			{' ',' ',' ',' ',' ',' '},
			{' ',' ',' ',' ',' ',' '},
			{' ',' ',' ',' ',' ',' '},
			{' ',' ',' ',' ',' ',' '},
			{' ',' ',' ',' ',' ',' '}};
			
        testCasJeuFini(plateau1, false);
        testCasJeuFini(plateau2, true);
    }

	/**
	* teste un appel de jeuFini
	* @param plateau tableau ou l'on vérifie si la partie est terminée
	* @param result resultat attendu
	*/
    void testCasJeuFini(char[][] plateau, boolean result){
		//Arrange
		System.out.println("Avec la fonction affichePlateau on peut voir le tableau ou la fonction jeuFini doit retourner \t: " + result + "\t : ");
		affichePlateau(plateau);
		
		//Act
		boolean resExec = jeuFini(plateau);

		//Assert
		if (resExec == result){
			System.out.println("OK");
		} else {
			System.err.println("ERREUR");
		}
    }
    
    //Méthodes utile pour les méthodes de test
    /**
	 * Permet d'afficher un tableau 
	 * @param t tableau à afficher
	 */
	void displayTab1(int[] t){ 
		int i = 0;
		System.out.print("{");
		while(i<t.length-1){
			System.out.print(t[i] + ",");
			i=i+1;
		}
		System.out.print(t[i]+"}");
	}
	
	/**
	 * Permet d'afficher un tableau à double dimension
	 * @param t tableau à afficher
	 */
	void displayTab(int[][] t){
		int i = 0;
		System.out.print("t = {");
		while(i<t.length-1){
			int j = 0;
			System.out.print("{");
			while(j<t[i].length-1){
				System.out.print(t[i][j]);
				System.out.print(",");
				j++;
			}
			System.out.print(t[i][j]+"}");
			System.out.print(",");
			i=i+1;
		}
		System.out.print("{");
		int j = 0;
		while(j<t[i].length-1){
			System.out.print(t[i][j]);
			System.out.print(",");
			j++;
		}
		System.out.print(t[i][j]+"}");
		System.out.print("}");
	}
}


